package constants;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class GoogleSearchConstant {

    public static final String searchField = "input[class*=\"gLFyf gsfi\"]";
    public static final String knowledgPanal = "div[class*=\"kp-wholepage kp-wholepage-osrp HSryR EyBRub\"]";
    public static final String reviewButton = "span[class*=\"hqzQac\"]";
    public static final String headingDiv = "div[class*=\"P5Bobd\"]";
    public static final String namediv = "div[class*=\"TSUbDb\"]";
    public static final String seePhoto = "div[class*=\"fWhgmd\"]";
    public static final String seePhoto1 = "span[class*=\"z3dsh\"]";
    public static final String nextPhoto = "div[class*=\"viewerfooter-navigation-button-next\"]";
    public static final String prevPhoto = "div[class*=\"viewerfooter-navigation-button-prev\"]";
    public static final String backImg = "img[class*=\"dAEYsb NTlx9b\"]";
    public static final String backImg1 = "div[class*=\"GVL9tf\"]";
    public static final String direction = "a[class*=\"ab_button\"]";
    public static final String mapLink = "h1[class*=\"section-directions-trip-title\"]";
    public static final String backBtn = "button[class*=\"section-trip-header-back mapsConsumerUiIconsCssCommon__maps-sprite-common-arrow-back-white\"]";
    public static final String expande1 = "button[class*=\"directions-group-disclose\"]";
    public static final String searchName = "q";
    public static String reviewScroll = "div[class*=\"FGlxyd\"]";
    public static String expandDirection = "button[class*=\"directions-group-disclose\"]";
    public static String websiteLink = "a[class*=\"ab_button\"]";
}
