package pages;

import constants.ThreeLayersConstant;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class ThreeLayers extends PageHelper{
    public ThreeLayers(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.CSS, using = ThreeLayersConstant.viewAll)
    public  static WebElement viewAll;

    @FindBy(how = How.CSS, using = ThreeLayersConstant.name1)
    public  static WebElement name1;

    @FindBy(how = How.CSS, using = ThreeLayersConstant.photo)
    public  static WebElement photo;

    @FindBy(how = How.CSS, using = ThreeLayersConstant.nextPhoto)
    public  static WebElement nextPhoto;

    @FindBy(how = How.CSS, using = ThreeLayersConstant.prevPhoto)
    public  static WebElement prevPhoto;

    @FindBy(how = How.CSS, using = ThreeLayersConstant.backbutton)
    public  static WebElement backbutton;

    @FindBy(how = How.CSS, using = ThreeLayersConstant.backbutton1)
    public  static WebElement backbutton1;

    @FindBy(how = How.CSS, using = ThreeLayersConstant.nextButton)
    public  static WebElement nextButton;

    @FindBy(how = How.CSS, using =  ThreeLayersConstant.prevButton)
    public  static WebElement prevButton;

    @FindBy(how = How.NAME, using = ThreeLayersConstant.searchName)
    public  static WebElement searchName;

    @FindBy(how = How.CSS, using = ThreeLayersConstant.viewAll)
    public  static WebElement direction;

    @FindBy(how = How.CSS, using = ThreeLayersConstant.hiDirection)
    public  static WebElement hiDirection;

    public void EnterkeyName() throws Exception {
        searchName.sendKeys(Keys.ENTER);
    }

}

