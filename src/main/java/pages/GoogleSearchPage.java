package pages;

import constants.GoogleSearchConstant;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class GoogleSearchPage extends PageHelper {
    public GoogleSearchPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.CSS, using = GoogleSearchConstant.searchField)
    public  static WebElement searchField;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.knowledgPanal)
    public  static WebElement knowledgPanal;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.reviewButton)
    public  static WebElement reviewButton;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.headingDiv)
    public  static WebElement headingDiv;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.namediv)
    public  static WebElement namediv;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.seePhoto)
    public  static WebElement seePhoto;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.seePhoto1)
    public  static WebElement seePhoto1;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.nextPhoto)
    public  static WebElement nextPhoto;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.prevPhoto)
    public  static WebElement prevPhoto;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.backImg)
    public  static WebElement backImg;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.backImg1)
    public  static WebElement backImg1;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.direction)
    public  static WebElement direction;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.mapLink)
    public  static WebElement mapLink;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.backBtn)
    public  static WebElement backBtn;

    @FindBy(how = How.CSS, using = GoogleSearchConstant.expande1)
    public  static WebElement expande1;

    @FindBy(how = How.NAME, using = GoogleSearchConstant.searchName)
    public  static WebElement searchName;

    public void clickEnter() throws Exception {
        searchField.sendKeys(Keys.ENTER);
    }

    public void clicnEscap() throws Exception {
        searchField.sendKeys(Keys.ESCAPE);
    }


}

