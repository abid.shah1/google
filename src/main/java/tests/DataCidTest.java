package tests;

import constants.ConstantVariable;
import constants.DataCidConstant;
import org.testng.annotations.Test;
import pages.DataCid;
import utils.ExtentTestManager;
import utils.PropertiesFile;

import java.lang.reflect.Method;

public class DataCidTest extends BaseTest {

    @Test(priority = 1)
    public void dataCidName(Method method) throws Exception {

        ExtentTestManager.startTest(method.getName(), "With Cid or Name");
        page.GetInstance(DataCid.class).sendKey(DataCid.searchName, PropertiesFile.readPropertiesFile("threeLaySearchText"));
        page.GetInstance(DataCid.class).checkANdClick(1, DataCidConstant.alert);
        page.GetInstance(DataCid.class).EnterkeyName();
        page.GetInstance(DataCid.class).verifyAtrribute("11668585010717378698", "Aazad Law Associates");//11668585010717378698   Aazad Law Associates
        ConstantVariable.methodeName = method.getName();
    }
}
