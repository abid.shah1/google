package tests;

import com.google.common.io.Files;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import pages.PageGenerator;
import pages.PageHelper;
import utils.PropertiesFile;
import pages.PageHelper;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class BaseTest {
    public static WebDriver driver;
    public WebDriverWait wait;
    public PageGenerator page;


    @BeforeClass(groups = {"Functionality", "Ui"})
    public void before_class() throws Exception {

        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<String, Object>();

        prefs.put("intl.accept_languages", "es");
        prefs.put("prompts.tab_modal.enabled", true);
          prefs.put("pdfjs.disabled", true);
    //    prefs.put("plugins.always_open_pdf_externally", true);
     //   prefs.put("profile.content_settings.plugin_whitelist", true);






        options.setExperimentalOption("prefs", prefs);


        System.out.println(System.getProperty("os.name"));
        if(System.getProperty("os.name").contains("Mac")){
            System.setProperty("webdriver.chrome.driver",
                    "chromeDriver/MacDriver/chromedriver");
            PageHelper.compress("extension/manifest.json","extension/background.js", "extension/extension.zip");
            options.addArguments("/Users/abidali/Library/Application Support/Google/Chrome/profile");
        }else{
            System.setProperty("webdriver.gecko.driver",
                    "\\chromeDriver\\WindowsDriver\\chromedriver.exe");
            options.addArguments("user-data-dir=C:\\Users\\Administrator\\AppData\\Local\\Google\\Chrome\\User Data\\profile2");
            PageHelper.compress("extension\\manifest.json","extension\\background.js", "extension\\extension.zip");
        }
        ///
        for (String optionAgrument : (new String[] {
                "--allow-insecure-localhost",
                "--allow-running-insecure-content",
                "--browser.download.folderList=2",
                "--browser.helperApps.neverAsk.saveToDisk=image/jpg,text/csv,text/xml,application/xml,application/vnd.ms-excel,application/x-excel,application/x-msexcel,application/excel,application/pdf",
                "--disable-blink-features=AutomationControlled",
                "--disable-default-app",
                "--disable-dev-shm-usage",
                "--disable-extensions",
                "--disable-gpu",
                "--disable-infobars",
                "--disable-in-process-stack-traces",
                "--disable-logging",
                "--disable-notifications",
                "--disable-popup-blocking",
                "--disable-save-password-bubble",
                "--disable-translate",
                "--disable-web-security",
                "--enable-local-file-accesses",
                "--ignore-certificate-errors",
                "--ignore-certificate-errors",
                "--ignore-ssl-errors=true",
                "--log-level=3",
                "--no-proxy-server",
                "--no-sandbox",
                "--output=/dev/null",
                "--ssl-protocol=any",
             //   "--lang=es",
                // "--start-fullscreen",
                // "--start-maximized" ,
                "--user-agent=Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20120101 Firefox/33.0",
                // String.format("--browser.download.dir=%s", downloadFilepath)
                /*
                 * "--user-data-dir=/path/to/your/custom/profile",
                 * "--profile-directory=name_of_custom_profile_directory",
                 */
        }))//fontlist plugin list
         //   options.addArguments(optionAgrument);
        options.addArguments("--origin-trial-disabled-features=SecurePaymentConfirmation");
        options.setExperimentalOption("excludeSwitches", new String[]{"--disable-hang-monitor","--disable-client-side-phishing-detection"
                ,"--disable-default-apps", "--disable-backgrounding-occluded-windows", "--disable-background-networking", "enable-automation", "--disable-popup-blocking",
                "--disable-prompt-on-repost", "--disable-sync", "--enable-logging", "--log-level", "--no-first-run", "--no-service-autorun",
                "--password-store", "--start-maximized", "--test-type", "--use-mock-keychain",
                "--log-level", "--start-maximized", "--user-data-", "--enable-blink-features", "--remote-debugging-port", "--disable-blink-features=AutomationControlled"});



        if(PropertiesFile.readPropertiesFile("isProxy").contains("true")){
            if(System.getProperty("os.name").contains("Mac")){
          //      options.addExtensions(new File("extension/extension.zip"));
          //      options.addArguments("--no-sandbox");
            }else{
                options.addExtensions(new File("extension\\extension.zip"));
                options.addArguments("--no-sandbox");
            }
        }

        String externalJS = Files.toString( new File("chromeDriver/MacDriver/javascriptFirefox.js"), Charset.forName("utf-8"));
        String externalJS2 = Files.toString( new File("chromeDriver/MacDriver/javascriptFirefox2.js"), Charset.forName("utf-8"));

// Execute, assume no arguments, and no value to return



        driver = new ChromeDriver(options);
        // Assume Guava, but whatever you use to load files...
        Object ignore = ((JavascriptExecutor) driver).executeScript(externalJS);
  //      Object ignore2 = ((JavascriptExecutor) driver).executeScript(externalJS2);

        driver.get("https://www.google.com/"); }

    public void setUpProxy(){

    }

    static void configureAuthWithExtension(String url, String username, String password) {
        driver.get("chrome-extension://enhldmjbphoeibbpdhmjkchohnidgnah/options.html");
        driver.findElement(By.id("url")).sendKeys(url);
        driver.findElement(By.id("username")).sendKeys(username);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.className("credential-form-submit")).click();
    }

    @BeforeTest(groups = {"Functionality", "Ui"})
    public void classLevelSetup() throws Exception {

    }

    @AfterClass
    public void afterclass(){
        driver.close();
    }

    @BeforeMethod(groups = {"Functionality", "Ui"})
    public void methodLevelSetup() throws InterruptedException {
        page = new PageGenerator(driver);
        Thread.sleep(200);
    }


}

