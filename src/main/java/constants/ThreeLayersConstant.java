package constants;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ThreeLayersConstant {

    public static final String viewAll = "span[class*=\"wUrVib\"]";
    public static final String name1 = "div[class*=\"t1IUkc\"]";
    public static final String photo = "div[class*=\"vwrQge\"]";
    public static final String nextPhoto = "div[class*=\"viewerfooter-navigation-button-next\"]";
    public static final String prevPhoto = "div[class*=\"viewerfooter-navigation-button-prev\"]";
    public static final String backbutton = "button[class*=\"w9bAxc\"]";
    public static final String backbutton1 = "div[class*=\"GVL9tf\"]";
    public static final String nextButton = "div[class*=\"viewerfooter-navigation-button-next\"]";
    public static final String prevButton = "viewerfooter-navigation-button-prev";
    public static final String searchName = "q";
    public static final String direction = "a[class*=\"CL9Uqc ab_button\"]";
    public static final String hiDirection = "h1[class*=\"section-directions-trip-title\"]";
    public static String ThreeDirection = "a[class*=\"CL9Uqc ab_button\"]";
    public static String alert = "div[class*=\"jyfHyd\"]";
}

