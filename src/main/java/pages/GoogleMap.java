package pages;

import constants.GoogleMapConstant;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class GoogleMap extends PageHelper{
    public GoogleMap(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.CSS, using = GoogleMapConstant.searchField)
    public  static WebElement searchField;

    @FindBy(how = How.CSS, using = GoogleMapConstant.reviewButton)
    public  static WebElement reviewButton;

    @FindBy(how = How.CSS, using = GoogleMapConstant.reviewScroll)
    public  static WebElement reviewScroll;

    @FindBy(how = How.CSS, using = GoogleMapConstant.backButton)
    public  static WebElement backButton;

    @FindBy(how = How.CSS, using = GoogleMapConstant.photos)
    public  static WebElement photos;

    @FindBy(how = How.CSS, using = GoogleMapConstant.nextphoto)
    public  static WebElement nextphoto;


    @FindBy(how = How.CSS, using = GoogleMapConstant.nextphoto1)
    public  static WebElement nextphoto1;

    @FindBy(how = How.CSS, using = GoogleMapConstant.prevphoto)
    public  static WebElement prevphoto;

    @FindBy(how = How.CSS, using = GoogleMapConstant.prevphoto1)
    public  static WebElement prevphoto1;

    @FindBy(how = How.CSS, using = GoogleMapConstant.backBtn2)
    public  static WebElement backBtn2;

    @FindBy(how = How.CSS, using =GoogleMapConstant.direction)
    public  static WebElement direction;

    @FindBy(how = How.NAME, using = GoogleMapConstant.searchName)
    public  static WebElement searchName;

    public void Enterkey() throws Exception {
        searchField.sendKeys(Keys.ENTER);
    }
}

