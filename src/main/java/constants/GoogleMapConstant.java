package constants;

public class GoogleMapConstant {

    public static final String searchField = "input[class*=\"tactile-searchbox-input\"]";

    public static final String reviewButton = "button[class*=\"widget-pane-link\"]";
    public static final String reviewScroll = "div[class*=\"section-review-title\"]";
    public static final String backButton = "button[class*=\"mdc-icon-button GmIconButton GmIconButtonDarkTheme\"]";
    public static final String photos = "button[class*=\"section-hero-header-image-hero widget-pane-fading widget-pane-fade-in section-hero-header-image-hero-clickable\"]";
    public static final String nextphoto = "button[class*=\"widget-play-right mapsConsumerUiCommonRipple__ripple-container widget-play-button\"]";
    public static final String nextphoto1 = "button[class*=\"mapsTactileClientPlay__widget-play-right mapsConsumerUiCommonRipple__ripple-container mapsTactileClientPlay__widget-play-button\"]";
    public static final String prevphoto = "button[class*=\"widget-play-left mapsConsumerUiCommonRipple__ripple-container widget-play-button\"]";
    public static final String prevphoto1 = "button[class*=\"mapsTactileClientPlay__widget-play-left mapsConsumerUiCommonRipple__ripple-container mapsTactileClientPlay__widget-play-button\"]";
    public static final String backBtn2 = "button[class*=\"mapsConsumerUiSubviewSectionGm2Topappbar__action-button-clickable\"]";
    public static final String direction = "div[class*=\"mapsConsumerUiSubviewSectionGm2Actionchip__taparea\"]";
    public static final String searchName = "q";
    public static String reviewScrollMap = "div[class*=\"section-review-title\"]";
    public static String alertMap = "div[class*=\"lssxud\"]";
}

