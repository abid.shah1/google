package utils;

import com.relevantcodes.extentreports.LogStatus;
import constants.ConstantVariable;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import tests.BaseTest;



public class TestListener implements ITestListener {
    private WebDriver driver;
    private static String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.println("I am in onTestSkipped method " + getTestMethodName(result) + " skipped");
        ConstantVariable.methodeName = getTestMethodName(result);
        //ExtentReports log operation for skipped tests.
        ExtentTestManager.getTest().log(LogStatus.SKIP, "Test Skipped");
        ExtentManager.getReporter().flush();
    }

    @Override
    public void onStart(ITestContext context) {
        System.out.println("I am in onStart method " + context.getName());
        context.setAttribute("WebDriver", this.driver);
    }

    @Override
    public void onFinish(ITestContext context) {
        System.out.println("I am in onFinish method " + context.getName());
        //Do tier down operations for extentreports reporting!
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
    }

    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("I am in onTestStart method " + getTestMethodName(result) + " start");
        ConstantVariable.methodeName = getTestMethodName(result);

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("I am in onTestSuccess method " + getTestMethodName(result) + " succeed");
        ConstantVariable.methodeName = getTestMethodName(result);
        //ExtentReports log operation for passed tests.
        ExtentTestManager.getTest().log(LogStatus.PASS, "Test Passed");
        ExtentManager.getReporter().flush();
    }
    @Override
    public void onTestFailure(ITestResult result) {

        System.out.println("I am in onTestFailure method " + getTestMethodName(result) + " failed");
        //  ConstentVariable.methodeName = getTestMethodName(result);
        //Get driver from BaseTest and assign to local webDriver variable.
        Object testClass = result.getInstance();
        WebDriver webDriver = ((BaseTest) testClass).driver;
        //Take base64Screenshot screenshot.

        //ExtentReports log and screenshot operations for failed tests.

        ExtentTestManager.getTest().log(LogStatus.FAIL, "Test Failed");

        //   tests.ExtentTestManager.getTest().addScreenCapture(base64Screenshot));

        // first letst get the annotation value from the filed test case.
        TestClass.ScriptMetaData meta = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestClass.ScriptMetaData.class);

        ExtentManager.getReporter().flush();
    }



    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        System.out.println("Test failed but it is in defined success ratio " + getTestMethodName(iTestResult));
    }
}

