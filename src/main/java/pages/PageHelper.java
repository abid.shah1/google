package pages;

import constants.GoogleSearchConstant;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.io.*;
import java.nio.file.*;
import java.util.zip.*;

public class PageHelper extends PageGenerator {


    //If we need we can use custom wait in BasePage and all page classes
    WebDriverWait wait = new WebDriverWait(this.driver,20 );

    public PageHelper(WebDriver driver) {
        super(driver);
    }
    public static void compress(String file1, String file2, String ouptPutPath) throws IOException {
        File file = new File(ouptPutPath);
        file.delete();
        List<String> srcFiles = Arrays.asList(file1, file2);
        FileOutputStream fos = new FileOutputStream(ouptPutPath);
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        for (String srcFile : srcFiles) {
            File fileToZip = new File(srcFile);
            FileInputStream fis = new FileInputStream(fileToZip);
            ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
            zipOut.putNextEntry(zipEntry);

            byte[] bytes = new byte[1024];
            int length;
            while((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
            fis.close();
        }
        zipOut.close();
        fos.close();

    }

    public  void checkANdClick(int index, String cddSelector) throws Exception {
        try {
            scrollUpDown(index, cddSelector);
        }catch(Exception exception){
            System.out.println("No suchElement exception faced");
        }
    }

    public  void clickWebsiteAndScroll(int index, String cddSelector) throws Exception {
        try {

            scrollUpDown(index, cddSelector);
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("window.scrollBy(0,700)", "");
            js.executeScript("window.scrollBy(0,-700)", "");
            List<WebElement> links=driver.findElements(By.tagName("a"));
           // System.out.println("no of links:" +links.size());

            for(int i=3;i<links.size();i++)
            {
                if(!(links.get(i).getText().isEmpty()))
                {
                    String currentUrl = driver.getCurrentUrl();
                    links.get(i).click();
                  String link=  links.get(i).getText();
                    js.executeScript("window.scrollBy(0,700)", "");
                    js.executeScript("window.scrollBy(0,-700)", "");

                    String newUrl = driver.getCurrentUrl();
                    if(currentUrl.contains(newUrl)){
                        links=driver.findElements(By.tagName("a"));
                }else{
                        driver.navigate().back();
                        links=driver.findElements(By.tagName("a"));
                    }
                }
            }
        }catch(Exception exception){
            System.out.println("No suchElement exception faced");
        }
    }


    public  void scroll(int index, String cddSelector) throws Exception {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        List<WebElement> locator = driver.findElements(By.cssSelector(cddSelector));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", locator.get(index));
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
    }

    public  void sendKey(WebElement element, String value ) throws Exception {
        wait.until(ExpectedConditions.visibilityOf(element));
        element.clear();
        element.sendKeys(value);
    }


    public void isdisplayed(WebElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
        element.isDisplayed();
    }

    public void clickCss(WebElement element) throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        // Thread.sleep(2000);
        element.click();
    }

    public void clickCssTry(WebElement element, String link) throws InterruptedException {
        try{

            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            // Thread.sleep(2000);
            element.click();
        }catch (NoSuchElementException elementException){
            System.out.println(link +" link not available");
        }
    }

    public void scrollUpDown(int index, String cddSelector) throws InterruptedException {
        WebElement element = driver.findElement(By.cssSelector((cddSelector)));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        List<WebElement> locator = driver.findElements(By.cssSelector(cddSelector));
        locator.get(index).click();
    }

    public void CLickIndex(int index, String cddSelector) throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        List<WebElement> locator = driver.findElements(By.cssSelector(cddSelector));
        locator.get(index).click();
    }


}

