package constants;

public class DataCidConstant {

    public static final  String listCss  = "div[class*=\"dbg0pd\"]";
    public static final String searchField = "input[class*=\"gsfi\"]";
    public static final String viewAll = "span[class*=\"wUrVib\"]";
    public static final String photo = "div[class*=\"vwrQge\"]";
    public static final String nextPhoto =  "div[class*=\"viewerfooter-navigation-button-next\"]";
    public static final String prevPhoto =  "div[class*=\"viewerfooter-navigation-button-prev\"]";
    public static final String backImg =  "img[class*=\"N98EWd oms55c\"]";
    public static final String hiDirection =  "h1[class*=\"section-directions-trip-title\"]";
    public static final String review =  "span[class*=\"hqzQac\"]";
    public static final String closeImg =  "div[class*=\"dtCYCd\"]";
    public static String datCid =  "data-cid";
    public static String reviewScroll = "div[class*=\"FGlxyd\"]";
    public static String expandDirection = "button[class*=\"directions-group-disclose\"]";
    public static String listCsss = "a[class*=\"C8TUKc rllt__link a-no-hover-decoration\"]";
    public static final String searchName = "q";
    public static final String alert = "div[class*=\"jyfHyd\"]";
}

