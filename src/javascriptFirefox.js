(function generatePluginArray() {
    const pluginData = [
        { name: "Chrome PDF Plugin", filename: "internal-pdf-viewer", description: "Portable Document Format" },
        { name: "Chrome PDF Viewer", filename: "mhjfbmdgcfjbbpaeojofohoefgiehjai", description: "" },
        { name: "Native Client", filename: "internal-nacl-plugin", description: "" },
    ]
    const pluginArray = []
    pluginData.forEach(p => {
        function FakePlugin () { return p }
        const plugin = new FakePlugin()
        Object.setPrototypeOf(plugin, Plugin.prototype);
        pluginArray.push(plugin)
    })
    Object.setPrototypeOf(pluginArray, PluginArray.prototype);
    return pluginArray
})()