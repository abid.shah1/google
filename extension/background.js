background_js = ""
var config = {
    mode: "fixed_servers",
    rules: {
        singleProxy: {
            scheme: "http",
            host: '@config.AustraliaConfig.properties.["host"]',
            port: parseInt(10001)
},
bypassList: ["localhost"]
}
};

chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

function callbackFn(details) {
    return {
        authCredentials: {
            username: '@config.AustraliaConfig.properties.["username"]',
            password: '@config.AustraliaConfig.properties.["password"]'
        }
    };
}

chrome.webRequest.onAuthRequired.addListener(
    callbackFn,
    {urls: ["<all_urls>"]},
    ['blocking']
);
"" % (PROXY_HOST, PROXY_PORT, PROXY_USER, PROXY_PASS)