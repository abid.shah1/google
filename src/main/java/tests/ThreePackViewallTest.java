package tests;

import constants.GoogleSearchConstant;
import constants.ThreeLayersConstant;
import pages.DataCid;
import pages.ThreeLayers;
import utils.PropertiesFile;
import org.testng.annotations.Test;

public class ThreePackViewallTest extends BaseTest{

    @Test(priority = 1, groups = {"Functionality", "Ui"})
    public void threeLaySearch() throws Exception {
        page.GetInstance(DataCid.class).checkANdClick(1, ThreeLayersConstant.alert);
        page.GetInstance(ThreeLayers.class).sendKey(ThreeLayers.searchName, PropertiesFile.readPropertiesFile("threeLaySearchText"));
        page.GetInstance(ThreeLayers.class).EnterkeyName();
        page.GetInstance(ThreeLayers.class).clickCss(ThreeLayers.viewAll);
        Thread.sleep(5000);
        if( PropertiesFile.readPropertiesFile("googleThreePackPhoto").contains("true")) {
            page.GetInstance(ThreeLayers.class).clickCss(ThreeLayers.name1);
            page.GetInstance(ThreeLayers.class).clickCssTry(ThreeLayers.photo, "Three layer Search photos");
            page.GetInstance(ThreeLayers.class).clickCss(ThreeLayers.nextPhoto);
            Thread.sleep(3000);
            page.GetInstance(ThreeLayers.class).clickCss(ThreeLayers.prevPhoto);
            Thread.sleep(3000);
            page.GetInstance(ThreeLayers.class).clickCss(ThreeLayers.backbutton1);
        }
        if( PropertiesFile.readPropertiesFile("googleThreePackDirection").contains("true")) {
            page.GetInstance(ThreeLayers.class).scrollUpDown(1, ThreeLayersConstant.ThreeDirection);
            page.GetInstance(ThreeLayers.class).clickCss(ThreeLayers.hiDirection);
            page.GetInstance(ThreeLayers.class).scrollUpDown(0, GoogleSearchConstant.expandDirection);
            page.GetInstance(ThreeLayers.class).scrollUpDown(1, GoogleSearchConstant.expandDirection);
        }
    }
}

