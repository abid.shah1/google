package utils;

import constants.ConstantVariable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.TimeUnit;

public class TestClass {

    ConstantVariable constentVariable = new ConstantVariable();
    private WebDriver driver;
    // private String baseUrl;
    @Retention(RetentionPolicy.RUNTIME)
    public @interface ScriptMetaData {

        boolean productionReady();
    }

    @BeforeClass()
    public void setUp() throws Exception {

//        File chromedriverExecutable = new File("drivers"+File.separator+"chromedriver");
//        System.getProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/chromedriver");
//        driver = new ChromeDriver();
//        driver.get(constentVariable.qaUrl);
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }


    @Test
    public void testCase() throws Exception {

        String searchText = "Selenium";

        driver.findElement(By.id("searchInput")).clear();
        driver.findElement(By.id("searchInput")).sendKeys(searchText);
        driver.findElement(By.xpath(
                "(.//*[normalize-space(text()) and normalize-space(.)='" + searchText + "'])[6]/following::i[1]"))
                .click();

        // Injecting a failure here to start the Jira issue creation.
        assertEquals(driver.findElement(By.id("firstHeading")).getText(), "HP ALM");
        System.out.println("Pass");
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
    }
}

