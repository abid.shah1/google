package pages;

import constants.DataCidConstant;
import constants.ThreeLayersConstant;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class DataCid extends PageHelper {



    @FindBy(how = How.CSS, using = DataCidConstant.listCss)
    public static WebElement listCss;

    @FindBy(how = How.CSS, using = DataCidConstant.searchField)
    public static WebElement searchField;

    @FindBy(how = How.CSS, using = DataCidConstant.viewAll)
    public static WebElement viewAll;

    @FindBy(how = How.CSS, using = DataCidConstant.photo)
    public static WebElement photo;

    @FindBy(how = How.CSS, using = DataCidConstant.nextPhoto)
    public static WebElement nextPhoto;

    @FindBy(how = How.CSS, using = DataCidConstant.prevPhoto)
    public static WebElement prevPhoto;

    @FindBy(how = How.CSS, using = DataCidConstant.backImg)
    public static WebElement backImg;

    @FindBy(how = How.CSS, using = DataCidConstant.hiDirection)
    public  static WebElement hiDirection;

    @FindBy(how = How.CSS, using = DataCidConstant.alert)
    public  static WebElement alert;

    @FindBy(how = How.CSS, using = DataCidConstant.review)
    public  static WebElement review;


    @FindBy(how = How.CSS, using = DataCidConstant.closeImg)
    public  static WebElement closeImg;

    @FindBy(how = How.NAME, using = DataCidConstant.searchName)
    public  static WebElement searchName;

    public DataCid(WebDriver driver) {
        super(driver);
    }

    public void verifyAtrribute(String dataCid, String name) throws Exception {
        name = name.toUpperCase();
        List<WebElement> links = driver.findElements(By.cssSelector(DataCidConstant.listCsss));
        int linkCount = links.size();
        for (int i = 0; i < linkCount; i++) {
            System.out.println(links.get(i).getAttribute(DataCidConstant.datCid));
            String linkText = links.get(i).getText();
            linkText = linkText.toUpperCase();
            if (links.get(i).getAttribute(DataCidConstant.datCid).contains(dataCid) || linkText.contains(name)) {
                links.get(i).click();
                afteCid();
                break;
            }
            viewAll.click();
            List<WebElement> links2 = driver.findElements(By.cssSelector(DataCidConstant.listCsss));
            int linkCount2 = links2.size();
            for (int j = 0; i < linkCount2; j++) {
                System.out.println(links2.get(j).getAttribute(DataCidConstant.datCid));
                linkText = links2.get(j).getText();
                linkText = linkText.toUpperCase();
                if (links2.get(j).getAttribute(DataCidConstant.datCid).contains(dataCid) || linkText.contains(name)) {
                    links2.get(j).click();
                    afteCid();
                    break;
                }

            }
        }
    }

    public void afteCid() throws Exception {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        photo.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        nextPhoto.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        prevPhoto.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        backImg.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        clickCss(review);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        scrollUpDown(8,DataCidConstant.reviewScroll);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        scroll(2,DataCidConstant.reviewScroll);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ESCAPE).build().perform();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        List<WebElement> locator = driver.findElements(By.cssSelector(ThreeLayersConstant.ThreeDirection));
        locator.get(1).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        hiDirection.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        List<WebElement> expend = driver.findElements(By.cssSelector(DataCidConstant.expandDirection));
        expend.get(0).click();
        scroll(1, DataCidConstant.expandDirection);
        expend.get(1).click();
    }

    public void EnterkeyName() throws Exception {
        searchName.sendKeys(Keys.ENTER);
    }

}

