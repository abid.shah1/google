package pages;


import org.testng.TestNG;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Main {

    public static String arg;
    char ch;
    public static void main(String[] args) {

        arg = args[0];
        arg = arg.toUpperCase();
        System.out.println(arg);


        // Create object of TestNG Class
        TestNG runner=new TestNG();

// Create a list of String
        List<String> suitefiles=new ArrayList<String>();

// Add xml file which you have to execute
        suitefiles.add("/Users/abidali/Documents/Aitomation/google-campaigns-java/testNG.xml");

// now set xml file for execution
        runner.setTestSuites(suitefiles);

// finally execute the runner using run method
        runner.run();
    }
}
