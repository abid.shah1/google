package tests;

import constants.ConstantVariable;
import constants.GoogleSearchConstant;
import constants.ThreeLayersConstant;
import org.testng.annotations.Test;
import pages.*;
import utils.ExtentTestManager;
import utils.PropertiesFile;

import java.lang.reflect.Method;

public class GoogleSearchTest extends BaseTest{



    @Test(priority = 1, groups = {"Functionality", "Ui"})
    public void search(Method method) throws Exception {
        ExtentTestManager.startTest(method.getName(), "Google search");
        page.GetInstance(DataCid.class).checkANdClick(1, ThreeLayersConstant.alert);
        page.GetInstance(GoogleSearchPage.class).sendKey(GoogleSearchPage.searchName, PropertiesFile.readPropertiesFile("searchText"));
        page.GetInstance(GoogleSearchPage.class).clickEnter();
        page.GetInstance(GoogleSearchPage.class).isdisplayed(GoogleSearchPage.knowledgPanal);
        page.GetInstance(GoogleSearchPage.class).clickWebsiteAndScroll(0,GoogleSearchConstant.websiteLink);

        if( PropertiesFile.readPropertiesFile("reviewGoogleSearchTest").contains("true")){
            page.GetInstance(GoogleSearchPage.class).clickCssTry(GoogleSearchPage.reviewButton, "Google search case review");
            page.GetInstance(GoogleSearchPage.class).clickCss(GoogleSearchPage.headingDiv);
            page.GetInstance(GoogleSearchPage.class).scrollUpDown(8, GoogleSearchConstant.reviewScroll);
            page.GetInstance(GoogleSearchPage.class).scrollUpDown(2, GoogleSearchConstant.reviewScroll);
            page.GetInstance(GoogleSearchPage.class).clicnEscap();
            page.GetInstance(GoogleSearchPage.class).sendKey(GoogleSearchPage.searchField, ConstantVariable.escape.toString());
        }
        if( PropertiesFile.readPropertiesFile("photoGoogleSearchTest").contains("true")) {
            page.GetInstance(GoogleSearchPage.class).clickCssTry(GoogleSearchPage.seePhoto1, "Google search case photo");
            page.GetInstance(GoogleSearchPage.class).clickCss(GoogleSearchPage.nextPhoto);
            page.GetInstance(GoogleSearchPage.class).clickCss(GoogleSearchPage.prevPhoto);
            page.GetInstance(GoogleSearchPage.class).clickCss(GoogleSearchPage.backImg1);
        }
        if( PropertiesFile.readPropertiesFile("directionGoogleSearchTest").contains("true")) {
            page.GetInstance(GoogleSearchPage.class).clickCss(GoogleSearchPage.direction);
            page.GetInstance(GoogleSearchPage.class).clickCssTry(GoogleSearchPage.mapLink, "Google search case map");
            page.GetInstance(GoogleSearchPage.class).scrollUpDown(0, GoogleSearchConstant.expandDirection);
            page.GetInstance(GoogleSearchPage.class).scrollUpDown(1, GoogleSearchConstant.expandDirection);
            page.GetInstance(GoogleSearchPage.class).clickCss(GoogleSearchPage.backBtn);
        }
        ConstantVariable.methodeName = method.getName();

    }
}

