package tests;

import constants.ConstantVariable;
import constants.GoogleMapConstant;
import org.testng.annotations.Test;
import pages.*;
import utils.ExtentTestManager;
import utils.PropertiesFile;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class GoogleMapTest extends BaseTest{

    @Test(priority = 1, groups = {"Functionality", "Ui"})
    public void searchMap(Method method) throws Exception {
        ExtentTestManager.startTest(method.getName(), "With Google Map ");
        driver.navigate().to(ConstantVariable.mapurl);
        page.GetInstance(DataCid.class).checkANdClick(1, GoogleMapConstant.alertMap);
        page.GetInstance(GoogleMap.class).sendKey(GoogleMap.searchName, PropertiesFile.readPropertiesFile("searchText"));
        page.GetInstance(GoogleMap.class).Enterkey();
        if( PropertiesFile.readPropertiesFile("googleMapReview").contains("true")) {
            page.GetInstance(GoogleMap.class).clickCssTry(GoogleMap.reviewButton, "Google Map case photo");
            page.GetInstance(GoogleMap.class).scroll(5, GoogleMapConstant.reviewScrollMap);
            page.GetInstance(GoogleMap.class).scroll(1, GoogleMapConstant.reviewScrollMap);
            Thread.sleep(4000);
            page.GetInstance(GoogleMap.class).clickCss(GoogleMap.backButton);
        }
        if( PropertiesFile.readPropertiesFile("googleMapPhotos").contains("true")) {
            page.GetInstance(GoogleMap.class).clickCssTry(GoogleMap.photos, "Google Map case photo");
            page.GetInstance(GoogleMap.class).clickCss(GoogleMap.nextphoto1);
            page.GetInstance(GoogleMap.class).clickCss(GoogleMap.prevphoto1);
            page.GetInstance(GoogleMap.class).clickCss(GoogleMap.backBtn2);
            page.GetInstance(GoogleMap.class).clickCss(GoogleMap.direction);
        }
        ConstantVariable.methodeName = method.getName();
    }


}

